package main

import (
	"database/sql"
	"fmt"
	"gideon/db_ops"
	"gideon/process_files"
	log "github.com/sirupsen/logrus"
	"os"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "iamtheeldenlord"
	dbname   = "postgres"
)

func main() {
	// reset the database
	db := connectToDB(host, port, user, password, dbname)
	defer closeDatabase(db)

	if os.Args[1] == "setup" {
		db_ops.ClearDBMakeTables(db)
		return
	}

	// get the watchlist
	chanStockQuotes := make(chan process_files.StockQuote)
	chanOptionStats := make(chan process_files.OptionStats)
	chanOptionQuotes := make(chan process_files.OptionQuote)
	chanDone := make(chan int)

	watchlist := process_files.ReadWatchlist(os.Args[1])

	go process_files.ProcessZipFile(os.Args[2], watchlist, chanStockQuotes, chanOptionStats, chanOptionQuotes, chanDone)

	db_ops.CollectResults(db, chanStockQuotes, chanOptionStats, chanOptionQuotes, chanDone)

	log.Debug("HI")
}

// Connects to the given database
// Panics if there is an error connecting
func connectToDB(host string, port int, user string, password string, dbname string) *sql.DB {
	// connection string
	psqlConnection := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	// open database
	db, err := sql.Open("postgres", psqlConnection)
	if err != nil {
		log.Fatal(err)
	}

	// check db connection
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	//log.Debug("Connected to Database!")

	return db
}

func closeDatabase(db *sql.DB) {
	err := db.Close()
	if err != nil {
		log.Fatal(err)
	}
}
