package process_files

import (
	"archive/zip"
	"encoding/csv"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

type StockQuote struct {
	Ticker   string
	Date     time.Time
	AdjClose float64
	Close    float64
}

type OptionStats struct {
	Ticker   string
	Date     time.Time
	IV30call float64
	IV30put  float64
	IV30mean float64
}

type OptionQuote struct {
	OccSymbol  string
	Underlying string
	Date       time.Time
	Bid        float64
	Ask        float64
	Last       float64
	IV         float64
	Delta      float64
	Gamma      float64
	Theta      float64
	Vega       float64
}

// ProcessZipFile takes the name of a zip file and a watchlist, and sends the relevant data out of the provided channels
// Panics if there is an error reading any of the files
func ProcessZipFile(filename string, watchlist map[string]bool, chanEquities chan<- StockQuote, chanOptionsData chan<- OptionStats, chanOptionQuotes chan<- OptionQuote, done chan<- int) {
	// open the zip file
	zipReader, err := zip.OpenReader(filename)
	if err != nil {
		log.Fatal("Error: '", err, "' File: '", filename, "'")
	}
	defer func(zipReader *zip.ReadCloser) {
		err := zipReader.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(zipReader)

	// iterate over the files in the zip archive
	for _, zipFile := range zipReader.File {
		// open up the csv file
		csvFile, err := zipFile.Open()
		if err != nil {
			log.Fatal(err)
		}

		// read csv data into format of `[][]string`
		csvReader := csv.NewReader(csvFile)
		records, err := csvReader.ReadAll()
		if err != nil {
			log.Fatal(err)
		}

		// write data to channels
		if strings.Contains(zipFile.Name, "_options_") {
			// handle equity quote
			handleOptionQuotes(records, watchlist, chanOptionQuotes)
		} else if strings.Contains(zipFile.Name, "_optionstats_") {
			// handle options data
			handleOptionStats(records, watchlist, chanOptionsData)
		} else if strings.Contains(zipFile.Name, "_stockquotes_") {
			// handle options quote
			handleStockQuotes(records, watchlist, chanEquities)
		} else {
			log.Fatal("Invalid CSV file name")
		}
	}

	done <- 1

	return
}

// Takes records from a CSV of options quotes, and passes those in the watchlist out to a channel of optionQuotes
func handleOptionQuotes(records [][]string, watchlist map[string]bool, outputChannel chan<- OptionQuote) {
	for _, quote := range records {
		if watchlist[quote[0]] {
			date, _ := time.Parse("1/2/2006", quote[7])
			bid, _ := strconv.ParseFloat(quote[10], 64)
			ask, _ := strconv.ParseFloat(quote[11], 64)
			last, _ := strconv.ParseFloat(quote[9], 64)
			iv, _ := strconv.ParseFloat(quote[14], 64)
			delta, _ := strconv.ParseFloat(quote[15], 64)
			gamma, _ := strconv.ParseFloat(quote[16], 64)
			theta, _ := strconv.ParseFloat(quote[17], 64)
			vega, _ := strconv.ParseFloat(quote[18], 64)
			quote := OptionQuote{
				OccSymbol:  quote[3],
				Underlying: quote[0],
				Date:       date,
				Bid:        bid,
				Ask:        ask,
				Last:       last,
				IV:         iv,
				Delta:      delta,
				Gamma:      gamma,
				Theta:      theta,
				Vega:       vega,
			}
			outputChannel <- quote
		}
	}
}

// Takes records from a CSV of options statistics, and passes those in the watchlist out to a channel of OptionsData
func handleOptionStats(records [][]string, watchlist map[string]bool, outputChannel chan<- OptionStats) {
	for _, quote := range records {
		if watchlist[quote[0]] {
			date, _ := time.Parse("1/2/2006", quote[1])
			iv30call, _ := strconv.ParseFloat(quote[2], 64)
			iv30put, _ := strconv.ParseFloat(quote[3], 64)
			iv30mean, _ := strconv.ParseFloat(quote[4], 64)
			quote := OptionStats{
				Ticker:   quote[0],
				Date:     date,
				IV30call: iv30call,
				IV30put:  iv30put,
				IV30mean: iv30mean,
			}
			outputChannel <- quote
		}
	}
}

// Takes records from a CSV of equity quotes, and passes those in the watchlist out to a channel of equityQuotes
func handleStockQuotes(records [][]string, watchlist map[string]bool, outputChannel chan<- StockQuote) {
	for _, quote := range records {
		if watchlist[quote[0]] {
			date, _ := time.Parse("1/2/2006", quote[1])
			adjClosePrice, _ := strconv.ParseFloat(quote[7], 64)
			closePrice, _ := strconv.ParseFloat(quote[5], 64)
			quote := StockQuote{
				Ticker:   quote[0],
				Date:     date,
				AdjClose: adjClosePrice,
				Close:    closePrice,
			}

			outputChannel <- quote
		}
	}
}

// ReadWatchlist reads in the watchlist from the specified filename, and returns it as a set/map of bools
// It can panic on file IO errors
func ReadWatchlist(filename string) map[string]bool {
	// open the file
	wl, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer func(wl *os.File) {
		err := wl.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(wl)

	// extract the watchlist
	csvReader := csv.NewReader(wl)
	watchlist, err := csvReader.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	// create the watchlist slice
	returnValue := make(map[string]bool)
	for _, ticker := range watchlist {
		returnValue[ticker[0]] = true
	}
	return returnValue
}
