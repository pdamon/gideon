package process_files

import (
	"encoding/csv"
	"github.com/stretchr/testify/assert"
	"log"
	"os"
	"testing"
	"time"
)

func TestReadWatchlist(t *testing.T) {
	watchlist := ReadWatchlist("./testdata/watchlist.csv")

	assert.True(t, watchlist["SPY"])
	assert.True(t, watchlist["IWM"])
	assert.True(t, watchlist["QQQ"])
	assert.Equal(t, len(watchlist), 3)
}

func TestReadZipFile(t *testing.T) {
	chanStockQuotes := make(chan StockQuote)
	chanOptionStats := make(chan OptionStats)
	chanOptionsQuotes := make(chan OptionQuote)
	chanDone := make(chan int)
	watchlist := ReadWatchlist("./testdata/watchlist.csv")

	go ProcessZipFile("./testdata/L2_2019_August.zip", watchlist, chanStockQuotes, chanOptionStats, chanOptionsQuotes, chanDone)

	augFirst2019, err := time.Parse("1/2/2006", "8/1/2019")
	if err != nil {
		log.Fatal(err)
	}
	augLast2019, err := time.Parse("1/2/2006", "8/30/2019")
	if err != nil {
		log.Fatal(err)
	}

	// check reading in the options data
	firstOptionStats := <-chanOptionStats
	var lastOptionStats OptionStats
	for i := 0; i < 65; i++ {
		lastOptionStats = <-chanOptionStats
	}

	assert.Equal(t, "IWM", firstOptionStats.Ticker)
	assert.Equal(t, augFirst2019, firstOptionStats.Date)
	assert.InDelta(t, 0.1914, firstOptionStats.IV30call, 0.00001)
	assert.InDelta(t, 0.1921, firstOptionStats.IV30put, 0.00001)
	assert.InDelta(t, 0.1917, firstOptionStats.IV30mean, 0.00001)

	assert.Equal(t, "SPY", lastOptionStats.Ticker)
	assert.Equal(t, augLast2019, lastOptionStats.Date)
	assert.InDelta(t, 0.1431, lastOptionStats.IV30call, 0.00001)
	assert.InDelta(t, 0.1816, lastOptionStats.IV30put, 0.00001)
	assert.InDelta(t, 0.1624, lastOptionStats.IV30mean, 0.00001)

	// check reading in the option quotes
	firstOptionQuote := <-chanOptionsQuotes
	var lastOptionQuote OptionQuote
	for i := 0; i < 290015; i++ {
		lastOptionQuote = <-chanOptionsQuotes
	}

	assert.Equal(t, "IWM190802C00125000", firstOptionQuote.OccSymbol)
	assert.Equal(t, "IWM", firstOptionQuote.Underlying)
	assert.Equal(t, augFirst2019, firstOptionQuote.Date)
	assert.InDelta(t, firstOptionQuote.Bid, 29.29, 0.00001)
	assert.InDelta(t, firstOptionQuote.Ask, 29.48, 0.00001)
	assert.InDelta(t, firstOptionQuote.Last, 30.89, 0.00001)
	assert.InDelta(t, firstOptionQuote.IV, 0.5462, 0.00001)
	assert.InDelta(t, firstOptionQuote.Delta, 1, 0.00001)
	assert.InDelta(t, firstOptionQuote.Gamma, 0, 0.00001)
	assert.InDelta(t, firstOptionQuote.Theta, -2.8711, 0.00001)
	assert.InDelta(t, firstOptionQuote.Vega, 0, 0.00001)

	assert.Equal(t, "SPY211217P00410000", lastOptionQuote.OccSymbol)
	assert.Equal(t, "SPY", lastOptionQuote.Underlying)
	assert.Equal(t, augLast2019, lastOptionQuote.Date)
	assert.InDelta(t, lastOptionQuote.Bid, 116.96, 0.00001)
	assert.InDelta(t, lastOptionQuote.Ask, 119.97, 0.00001)
	assert.InDelta(t, lastOptionQuote.Last, 125, 0.00001)
	assert.InDelta(t, lastOptionQuote.IV, 0.285, 0.00001)
	assert.InDelta(t, lastOptionQuote.Delta, -0.6713, 0.00001)
	assert.InDelta(t, lastOptionQuote.Gamma, 0.0029, 0.00001)
	assert.InDelta(t, lastOptionQuote.Theta, -2.7071, 0.00001)
	assert.InDelta(t, lastOptionQuote.Vega, 160.3567, 0.00001)

	// check reading in the stock quotes
	firstStockQuote := <-chanStockQuotes
	var lastStockQuote StockQuote
	for i := 0; i < 65; i++ {
		lastStockQuote = <-chanStockQuotes
	}

	assert.Equal(t, "IWM", firstStockQuote.Ticker)
	assert.Equal(t, augFirst2019, firstStockQuote.Date)
	assert.InDelta(t, 154.31, firstStockQuote.AdjClose, 0.00001)
	assert.InDelta(t, 154.31, firstStockQuote.Close, 0.00001)

	assert.Equal(t, "SPY", lastStockQuote.Ticker)
	assert.Equal(t, augLast2019, lastStockQuote.Date)
	assert.InDelta(t, 292.4527, lastStockQuote.AdjClose, 0.00001)
	assert.InDelta(t, 292.4527, lastStockQuote.Close, 0.00001)

	assert.Equal(t, 1, <-chanDone)
}

func TestHandleOptionQuotes(t *testing.T) {
	// make test objects
	watchlist := ReadWatchlist("./testdata/watchlist.csv")
	outputChan := make(chan OptionQuote)

	// open the test file
	csvFile, err := os.Open("./testdata/L2_options_20190801.csv")
	if err != nil {
		log.Fatal(err)
	}
	csvReader := csv.NewReader(csvFile)
	records, err := csvReader.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	// read the test records
	go handleOptionQuotes(records, watchlist, outputChan)

	// check the test output
	first := <-outputChan
	var last OptionQuote
	for i := 0; i < 12619; i++ {
		last = <-outputChan
	}

	augFirst2019, err := time.Parse("1/2/2006", "8/1/2019")
	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, "IWM190802C00125000", first.OccSymbol)
	assert.Equal(t, "IWM", first.Underlying)
	assert.Equal(t, augFirst2019, first.Date)
	assert.InDelta(t, first.Bid, 29.29, 0.00001)
	assert.InDelta(t, first.Ask, 29.48, 0.00001)
	assert.InDelta(t, first.Last, 30.89, 0.00001)
	assert.InDelta(t, first.IV, 0.5462, 0.00001)
	assert.InDelta(t, first.Delta, 1, 0.00001)
	assert.InDelta(t, first.Gamma, 0, 0.00001)
	assert.InDelta(t, first.Theta, -2.8711, 0.00001)
	assert.InDelta(t, first.Vega, 0, 0.00001)

	assert.Equal(t, "SPY211217P00410000", last.OccSymbol)
	assert.Equal(t, "SPY", last.Underlying)
	assert.Equal(t, augFirst2019, last.Date)
	assert.InDelta(t, last.Bid, 113.07, 0.00001)
	assert.InDelta(t, last.Ask, 116.69, 0.00001)
	assert.InDelta(t, last.Last, 107.96, 0.00001)
	assert.InDelta(t, last.IV, 0.2723, 0.00001)
	assert.InDelta(t, last.Delta, -0.6714, 0.00001)
	assert.InDelta(t, last.Gamma, 0.0029, 0.00001)
	assert.InDelta(t, last.Theta, -2.227, 0.00001)
	assert.InDelta(t, last.Vega, 164.507, 0.00001)
}

func TestHandleStockQuotes(t *testing.T) {
	// make test objects
	watchlist := ReadWatchlist("./testdata/watchlist.csv")
	outputChan := make(chan StockQuote)

	// open the test file
	csvFile, err := os.Open("./testdata/L2_stockquotes_20190801.csv")
	if err != nil {
		log.Fatal(err)
	}
	csvReader := csv.NewReader(csvFile)
	records, err := csvReader.ReadAll()
	if err != nil {
		log.Fatal(err)
	}
	augFirst2019, err := time.Parse("1/2/2006", "8/1/2019")
	if err != nil {
		log.Fatal(err)
	}

	// read the test records
	go handleStockQuotes(records, watchlist, outputChan)

	// check the test output
	iwm := <-outputChan
	qqq := <-outputChan
	spy := <-outputChan

	assert.Equal(t, "IWM", iwm.Ticker)
	assert.Equal(t, augFirst2019, iwm.Date)
	assert.InDelta(t, iwm.Close, 154.31, 0.00001)
	assert.InDelta(t, iwm.AdjClose, 154.31, 0.00001)

	assert.Equal(t, "QQQ", qqq.Ticker)
	assert.Equal(t, augFirst2019, qqq.Date)
	assert.InDelta(t, qqq.Close, 190.15, 0.00001)
	assert.InDelta(t, qqq.AdjClose, 190.15, 0.00001)

	assert.Equal(t, "SPY", spy.Ticker)
	assert.Equal(t, augFirst2019, spy.Date)
	assert.InDelta(t, spy.Close, 295, 0.00001)
	assert.InDelta(t, spy.AdjClose, 295, 0.00001)
}

func TestHandleOptionStats(t *testing.T) {
	// make test objects
	watchlist := ReadWatchlist("./testdata/watchlist.csv")
	outputChan := make(chan OptionStats)

	// open the test file
	csvFile, err := os.Open("./testdata/L2_optionstats_20190801.csv")
	if err != nil {
		log.Fatal(err)
	}
	csvReader := csv.NewReader(csvFile)
	records, err := csvReader.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	// read the test records
	go handleOptionStats(records, watchlist, outputChan)

	// check the test output
	iwm := <-outputChan
	qqq := <-outputChan
	spy := <-outputChan

	assert.Equal(t, "IWM", iwm.Ticker)
	assert.Equal(t, "QQQ", qqq.Ticker)
	assert.Equal(t, "SPY", spy.Ticker)

	augFirst2019, err := time.Parse("1/2/2006", "8/1/2019")
	if err != nil {
		log.Fatal(err)
	}
	assert.Equal(t, augFirst2019, iwm.Date)
	assert.Equal(t, augFirst2019, qqq.Date)
	assert.Equal(t, augFirst2019, spy.Date)

	assert.InDelta(t, iwm.IV30call, 0.1914, 0.00001)
	assert.InDelta(t, qqq.IV30call, 0.1976, 0.00001)
	assert.InDelta(t, spy.IV30call, 0.1554, 0.00001)

	assert.InDelta(t, iwm.IV30put, 0.1921, 0.00001)
	assert.InDelta(t, qqq.IV30put, 0.1885, 0.00001)
	assert.InDelta(t, spy.IV30put, 0.1533, 0.00001)

	assert.InDelta(t, iwm.IV30mean, 0.1917, 0.00001)
	assert.InDelta(t, qqq.IV30mean, 0.1930, 0.00001)
	assert.InDelta(t, spy.IV30mean, 0.1543, 0.00001)
}
