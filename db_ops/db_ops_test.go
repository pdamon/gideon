package db_ops

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestSplitOptionString(t *testing.T) {
	u, e, s, i := SplitOptionString("A190816C00042500")

	t1, _ := time.Parse("2006-01-02", "2019-08-16")
	assert.Equal(t, "A", u)
	assert.Equal(t, t1, e)
	assert.InDelta(t, 42.5, s, 0.00001)
	assert.True(t, i)

	u, e, s, i = SplitOptionString("PTCT200117P00060000")
	t2, _ := time.Parse("2006-01-02", "2020-01-17")

	assert.Equal(t, "PTCT", u)
	assert.Equal(t, t2, e)
	assert.InDelta(t, 60, s, 0.00001)
	assert.False(t, i)
}

func TestMakeTickersQuery(t *testing.T) {
	tickers := map[string]bool{"IWM": true}

	assert.Equal(t, "INSERT INTO stock_tickers VALUES ('IWM') ON CONFLICT DO NOTHING", makeTickersQuery(tickers))

	tickers["SPY"] = true
	tickers["QQQ"] = true

	assert.Equal(t, "INSERT INTO stock_tickers VALUES ('IWM'), ('QQQ'), ('SPY') ON CONFLICT DO NOTHING", makeTickersQuery(tickers))
}

func TestMakeExpirationsQuery(t *testing.T) {
	tickers := map[time.Time]bool{time.Date(2020, 3, 15, 0, 0, 0, 0, time.UTC): true}

	assert.Equal(t, "INSERT INTO option_expirations VALUES ('2020-03-15') ON CONFLICT DO NOTHING", makeExpirationsQuery(tickers))

	tickers[time.Date(2021, 12, 25, 0, 0, 0, 0, time.UTC)] = true
	tickers[time.Date(2022, 4, 20, 0, 0, 0, 0, time.UTC)] = true

	assert.Equal(t, "INSERT INTO option_expirations VALUES ('2020-03-15'), ('2021-12-25'), ('2022-04-20') ON CONFLICT DO NOTHING", makeExpirationsQuery(tickers))
}

func TestMakeOptionSymbolsQuery(t *testing.T) {
	symbols := map[string]string{"QAA200420C00420000": "AAPL"}

	assert.Equal(t, "INSERT INTO option_symbols VALUES ('QAA200420C00420000', 'AAPL', '2020-04-20', 420.000000, true) ON CONFLICT DO NOTHING", makeOptionSymbolsQuery(symbols))

	symbols["SPY221225P00400000"] = "SPY"
	symbols["QQQ080630P00215500"] = "QQQ"

	assert.Equal(t, "INSERT INTO option_symbols VALUES ('QAA200420C00420000', 'AAPL', '2020-04-20', 420.000000, true), "+
		"('QQQ080630P00215500', 'QQQ', '2008-06-30', 215.500000, false), "+
		"('SPY221225P00400000', 'SPY', '2022-12-25', 400.000000, false) ON CONFLICT DO NOTHING", makeOptionSymbolsQuery(symbols))

}
