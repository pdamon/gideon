package db_ops

import (
	"database/sql"
	"fmt"
	"gideon/process_files"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"slices"
	"sort"
	"strconv"
	"time"
)

type FullStockQuote struct {
	Ticker   string
	Date     time.Time
	AdjClose float64
	Close    float64
	IV30call float64
	IV30put  float64
	IV30mean float64
}

// ClearDBMakeTables clears the database by dropping the existing tables, and then creating new blank ones.
func ClearDBMakeTables(db *sql.DB) {
	// reset the database
	log.Debug("Dropping tables to reset")
	resetDataBase(db)

	// set up the database
	log.Debug("Setting Up Database")
	setupDatabase(db)
	log.Debug("Database Setup Complete.")
}

// SetupDatabase sets up the database.
// It panics if there is an error executing one of the queries.
func setupDatabase(db *sql.DB) {
	// tickers
	dbExecPanicOnErr(db,
		`CREATE TABLE stock_tickers
(
    ticker VARCHAR(6) NOT NULL,
    PRIMARY KEY (ticker)
)`)

	// expirations
	dbExecPanicOnErr(db, `CREATE TABLE option_expirations
(
    date DATE NOT NULL,
    PRIMARY KEY (date)
)`)

	// option symbols
	dbExecPanicOnErr(db, `CREATE TABLE option_symbols
(
    occ_symbol VARCHAR(21) NOT NULL,
    underlying VARCHAR(6)  NOT NULL,
    expiration DATE        NOT NULL,
    strike     REAL        NOT NULL,
    is_call    BOOLEAN     NOT NULL,
    PRIMARY KEY (occ_symbol),
    FOREIGN KEY (underlying) REFERENCES stock_tickers (ticker),
    FOREIGN KEY (expiration) REFERENCES option_expirations (date)
)`)

	// equity EOD quotes
	dbExecPanicOnErr(db, `CREATE TABLE stock_eod
(
    ticker    VARCHAR(6) NOT NULL,
    date      DATE       NOT NULL,
    adj_close REAL,
    close     REAL,
    iv30call  REAL,
    iv30put   REAL,
    iv30mean  REAL,
    PRIMARY KEY (ticker, date),
    FOREIGN KEY (ticker) REFERENCES stock_tickers (ticker)
)`)

	// option EOD quotes
	dbExecPanicOnErr(db, `CREATE TABLE options_eod
(
    occ_symbol    VARCHAR(21) NOT NULL,
    date          DATE        NOT NULL,
    bid           REAL        NOT NULL,
    ask           REAL        NOT NULL,
    last          REAL        NOT NULL,
    iv            REAL        NOT NULL,
    delta         REAL        NOT NULL,
    gamma         REAL        NOT NULL,
    theta         REAL        NOT NULL,
    vega          REAL        NOT NULL,
    PRIMARY KEY (occ_symbol, date),
    FOREIGN KEY (occ_symbol) REFERENCES option_symbols (occ_symbol)
)`)
}

// Resets the database by dropping tables.
// Panics if there is an error in one of the queries.
func resetDataBase(db *sql.DB) {
	dbExecPanicOnErr(db, `DROP TABLE IF EXISTS options_eod`)
	dbExecPanicOnErr(db, `DROP TABLE IF EXISTS stock_eod`)
	dbExecPanicOnErr(db, `DROP TABLE IF EXISTS option_symbols`)
	dbExecPanicOnErr(db, `DROP TABLE IF EXISTS option_expirations`)
	dbExecPanicOnErr(db, `DROP TABLE IF EXISTS stock_tickers`)
}

// Executes a SQL query.
// Panics if there is an error executing the query.
func dbExecPanicOnErr(db *sql.DB, query string) {
	result, err := db.Exec(query)
	if err != nil {
		log.Fatal("Error: '", err, "' Result: '", result, "' Query: '", query, "'")
	}
}

func CollectResults(db *sql.DB, chanStockQuotes <-chan process_files.StockQuote, chanOptionStats <-chan process_files.OptionStats, chanOptionQuotes <-chan process_files.OptionQuote, chanDone <-chan int) {
	stocksSlice := make([]process_files.StockQuote, 0)
	statsSlice := make([]process_files.OptionStats, 0)
	optionsSlice := make([]process_files.OptionQuote, 0)
	tickers := map[string]bool{}
	expirations := map[time.Time]bool{}
	optionSymbols := map[string]string{}

	finished := false
	for !finished {
		select {
		case stockQuote := <-chanStockQuotes:
			stocksSlice = append(stocksSlice, stockQuote)
			tickers[stockQuote.Ticker] = true
		case optionQuote := <-chanOptionQuotes:
			optionsSlice = append(optionsSlice, optionQuote)
			_, e, _, _ := SplitOptionString(optionQuote.OccSymbol)
			tickers[optionQuote.Underlying] = true
			expirations[e] = true
			optionSymbols[optionQuote.OccSymbol] = optionQuote.Underlying
		case optionStats := <-chanOptionStats:
			statsSlice = append(statsSlice, optionStats)
			tickers[optionStats.Ticker] = true
		case _ = <-chanDone:
			finished = true
		}
	}

	joinedQuotes := joinQuotesAndStats(statsSlice, stocksSlice)

	dbExecPanicOnErr(db, makeTickersQuery(tickers))
	log.Debug("Wrote new Tickers")
	dbExecPanicOnErr(db, makeExpirationsQuery(expirations))
	log.Debug("Wrote new Expirations")
	dbExecPanicOnErr(db, makeOptionSymbolsQuery(optionSymbols))
	log.Debug("Wrote new Option Symbols")
	dbExecPanicOnErr(db, makeStocksQuery(joinedQuotes))
	log.Debug("Wrote new Stock Quotes")
	optionsQueries := makeOptionsQuery(optionsSlice, 10000)
	for _, q := range optionsQueries {
		dbExecPanicOnErr(db, q)
	}
	log.Debug("Wrote new Option Quotes")
}

func makeTickersQuery(tickers map[string]bool) string {
	sortedTickers := make([]string, 0)
	for ticker := range tickers {
		sortedTickers = append(sortedTickers, ticker)
	}

	slices.Sort(sortedTickers)

	q := ""
	for _, ticker := range sortedTickers {
		q = q + fmt.Sprintf(" ('%s'),", ticker)
	}

	return "INSERT INTO stock_tickers VALUES" + q[:len(q)-1] + " ON CONFLICT DO NOTHING"
}

func makeExpirationsQuery(tickers map[time.Time]bool) string {
	sortedExpirations := make([]time.Time, 0)
	for expiration := range tickers {
		sortedExpirations = append(sortedExpirations, expiration)
	}

	sort.Slice(sortedExpirations, func(i, j int) bool {
		return sortedExpirations[i].Before(sortedExpirations[j])
	})

	q := ""
	for _, expiration := range sortedExpirations {
		q = q + fmt.Sprintf(" ('%s'),", expiration.Format("2006-01-02"))
	}

	return "INSERT INTO option_expirations VALUES" + q[:len(q)-1] + " ON CONFLICT DO NOTHING"
}

func makeOptionSymbolsQuery(symbols map[string]string) string {
	sortedSymbols := make([]string, 0)
	for symbol := range symbols {
		sortedSymbols = append(sortedSymbols, symbol)
	}

	slices.Sort(sortedSymbols)

	q := ""
	for _, symbol := range sortedSymbols {
		_, e, s, c := SplitOptionString(symbol)
		q = q + fmt.Sprintf(" ('%s', '%s', '%s', %f, %t),", symbol, symbols[symbol], e.Format("2006-01-02"), s, c)
	}

	return "INSERT INTO option_symbols VALUES" + q[:len(q)-1] + " ON CONFLICT DO NOTHING"
}

func makeStocksQuery(stocks []FullStockQuote) string {
	q := ""
	for _, stock := range stocks {
		q = q + fmt.Sprintf(" ('%s', '%s', %f, %f, %f, %f, %f),",
			stock.Ticker,
			stock.Date.Format("2006-01-02"),
			stock.AdjClose,
			stock.Close,
			stock.IV30call,
			stock.IV30put,
			stock.IV30mean)
	}

	return "INSERT INTO stock_eod VALUES" + q[:len(q)-1]
}

func makeOptionsQuery(quotes []process_files.OptionQuote, batchSize int) (rv []string) {
	for i := 0; i < len(quotes); i += batchSize {
		end := i + batchSize
		if end > len(quotes) {
			end = len(quotes)
		}
		q := ""
		for _, quote := range quotes[i:end] {
			q = q + fmt.Sprintf(" ('%s', '%s', %f, %f, %f, %f, %f, %f, %f, %f),",
				quote.OccSymbol,
				quote.Date.Format("2006-01-02"),
				quote.Bid,
				quote.Ask,
				quote.Last,
				quote.IV,
				quote.Delta,
				quote.Gamma,
				quote.Theta,
				quote.Vega)
		}

		rv = append(rv, "INSERT INTO options_eod VALUES"+q[:len(q)-1])
	}
	return rv
}

// Do it in N^2 time
func joinQuotesAndStats(stats []process_files.OptionStats, quotes []process_files.StockQuote) (rv []FullStockQuote) {
	for _, stat := range stats {
		for _, quote := range quotes {
			if stat.Ticker == quote.Ticker && stat.Date == quote.Date {
				fq := FullStockQuote{
					Ticker:   stat.Ticker,
					Date:     stat.Date,
					AdjClose: quote.AdjClose,
					Close:    quote.Close,
					IV30call: stat.IV30call,
					IV30put:  stat.IV30put,
					IV30mean: stat.IV30mean,
				}

				rv = append(rv, fq)
				break
			}
		}
	}

	return rv
}

// SplitOptionString splits an options string into constituent data parts.
// This string must have the format `[Ticker]YYMMDD[P/C]XXXXXDDD`.
// It returns them as `(underlying, expiration, strike, isCall)`
// Panics if there is an issue splitting the int
func SplitOptionString(occSymbol string) (string, time.Time, float64, bool) {
	l := len(occSymbol)
	underlying := occSymbol[0:(l - 15)]
	expiration, err := time.Parse("060102", occSymbol[(l-15):(l-9)])

	if err != nil {
		log.Fatal(err)
	}

	intVal, err := strconv.Atoi(occSymbol[(l - 8):(l)])
	if err != nil {
		log.Fatal(err)
	}

	strike := float64(intVal) / 1000
	isCall := occSymbol[l-9] == 'C'

	return underlying, expiration, strike, isCall
}
