# Gideon

Sir Gideon Ofnir, the all-knowing of options data.

## Usage

Run it using GNU Parallel

```bash
./gideon setup
ls ./DeltaNeutral/ | tac | parallel -j 12 --bar './gideon watchlist.csv ./DeltaNeutral/{}'
```

`tac` reverses the order of the files, so that the newest, largest ones get processed first.

### Tables

#### stock_tickers

* ticker VARCHAR(6) *[PK]*

#### option_expirations

* date DATE *[PK]*

#### option_symbols

* occ_symbol VARCHAR(21) *[PK]*
* underlying VARCHAR(6) *[FK]* equity_tickers(ticker)
* expiration DATE *[FK]* expirations(date)
* strike REAL
* is_call BOOLEAN

#### stock_eod

* ticker VARCHAR(6) *[PK]* *[FK]* equity_tickers(ticker)
* date DATE *[PK]*
* adj_close REAL
* close REAL
* iv30call REAL
* iv30put REAL
* iv30mean REAL

#### options_eod

* occ_symbol VARCHAR(21) *[PK]* *[FK]* option_symbols(occ_symbol)
* date DATE *[PK]*
* bid REAL
* ask REAL
* last REAL
* iv REAL
* delta REAL
* gamma REAL
* theta REAL
* vega REAL

## License

Copyright Parker Damon 2023 All Rights Reserved